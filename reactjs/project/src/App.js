import React from 'react';
import { BrowserRouter as Router, Route, Routes, useLocation } from 'react-router-dom';
import RegistrationForm from './Components/Register.js';
import MyNavbar from './Components/Navbar.js';
import Login from './Components/Login.js';
import MedicalStore from './Components/medicine.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import Contact from './Components/Contact.js';
import ContactDoctor from './Components/contactDoctor.js';
import './App.css';


function Content() {
  const location = useLocation();
  return (
    <div className="App">
        {location.pathname !== '/Register' && location.pathname !== '/Login' && 
        location.pathname !== '/medicine' && 
        location.pathname !== '/Contact'
         && 
         location.pathname !== '/contactDoctor' &&
         <MyNavbar />}
        <Routes>
            <Route path="/" element={<div>Homepage content or any default component goes here.</div>} />
            <Route path="/Register" element={<RegistrationForm />} />
            <Route path="/Login" element={<Login />} />
            <Route path="/medicine" element={<MedicalStore />} />
            <Route path="/contactDoctor" element={<ContactDoctor/>}/>
            <Route path="/Contact" element={<Contact />} />
        </Routes>
    </div>
);
}
function App() {
  return (
      <Router>
          <Routes>
              <Route path="*" element={<Content />} />
          </Routes>
      </Router>
  );
}

export default App;

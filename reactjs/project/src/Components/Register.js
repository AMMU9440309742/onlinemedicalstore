

// import React, { useState } from 'react';
// import './register.css';
// import { FaUser, FaPhone, FaBirthdayCake, FaEnvelope, FaLock } from 'react-icons/fa'; // Importing icons

// function RegistrationForm() {
//     const [formData, setFormData] = useState({
//         username: '',
//         phoneNumber: '',
//         age: '',
//         email: '',
//         password: ''
//     });

//     const handleChange = (e) => {
//         setFormData({
//             ...formData,
//             [e.target.name]: e.target.value
//         });
//     };

//     const handleSubmit = (e) => {
//         e.preventDefault();
//         console.log('Form data submitted:', formData);
//     };

//     return (
//         <div className="registration-container">
//             <form className="registration-form" onSubmit={handleSubmit}>
//                 <div className="input-container">
//                     <FaUser />
//                     <input 
//                         type="text" 
//                         name="username" 
//                         placeholder="Username" 
//                         value={formData.username} 
//                         onChange={handleChange} 
//                     />
//                 </div>
//                 <div className="input-container">
//                     <FaPhone />
//                     <input 
//                         type="tel" 
//                         name="phoneNumber" 
//                         placeholder="Phone Number" 
//                         value={formData.phoneNumber} 
//                         onChange={handleChange} 
//                     />
//                 </div>
//                 <div className="input-container">
//                     <FaBirthdayCake />
//                     <input 
//                         type="number" 
//                         name="age" 
//                         placeholder="Age" 
//                         value={formData.age} 
//                         onChange={handleChange} 
//                     />
//                 </div>
//                 <div className="input-container">
//                     <FaEnvelope />
//                     <input 
//                         type="email" 
//                         name="email" 
//                         placeholder="Email" 
//                         value={formData.email} 
//                         onChange={handleChange} 
//                     />
//                 </div>
//                 <div className="input-container">
//                     <FaLock />
//                     <input 
//                         type="password" 
//                         name="password" 
//                         placeholder="Password" 
//                         value={formData.password} 
//                         onChange={handleChange} 
//                     />
//                 </div>
//                 <button type="submit">Register</button>
//             </form>
//         </div>
//     );
// }

// export default RegistrationForm;

import React, { useState } from 'react';
import './register.css';
import { FaUser, FaPhone, FaBirthdayCake, FaEnvelope, FaLock, FaEye, FaEyeSlash } from 'react-icons/fa';

function RegistrationForm() {
    const [formData, setFormData] = useState({
        username: '',
        phoneNumber: '',
        age: '',
        email: '',
        password: '',
        confirmPassword: ''
    });

    const [showPassword, setShowPassword] = useState(false);

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Form data submitted:', formData);
    };

    return (
        <div className="registration-container">
            <img src="/path/to/your/logo.png" alt="Brand Logo" className="brand-logo" />
            <h2>Create an Account</h2>
            <form className="registration-form" onSubmit={handleSubmit}>
                <div className="input-container">
                    <FaUser />
                    <input 
                        type="text" 
                        name="username" 
                        placeholder="Username" 
                        value={formData.username} 
                        onChange={handleChange} 
                        required
                    />
                </div>
                <div className="input-container">
                    <FaPhone />
                    <input 
                        type="tel" 
                        name="phoneNumber" 
                        placeholder="Phone Number" 
                        value={formData.phoneNumber} 
                        onChange={handleChange} 
                        required
                    />
                </div>
                <div className="input-container">
                    <FaBirthdayCake />
                    <input 
                        type="number" 
                        name="age" 
                        placeholder="Age" 
                        value={formData.age} 
                        onChange={handleChange} 
                        required
                    />
                </div>
                <div className="input-container">
                    <FaEnvelope />
                    <input 
                        type="email" 
                        name="email" 
                        placeholder="Email" 
                        value={formData.email} 
                        onChange={handleChange} 
                        required
                    />
                </div>
                <div className="input-container">
                    <FaLock />
                    <input 
                        type={showPassword ? "text" : "password"}
                        name="password"
                        placeholder="Password"
                        value={formData.password}
                        onChange={handleChange}
                        required
                    />
                    <div className="toggle-password-icon" onClick={togglePasswordVisibility}>
                        {showPassword ? <FaEyeSlash /> : <FaEye />}
                    </div>
                </div>
                <div className="input-container">
                    <FaLock />
                    <input 
                        type={showPassword ? "text" : "password"}
                        name="confirmPassword"
                        placeholder="Confirm Password"
                        value={formData.confirmPassword}
                        onChange={handleChange}
                    />
                    <div className="toggle-password-icon" onClick={togglePasswordVisibility}>
                        {showPassword ? <FaEyeSlash /> : <FaEye />}
                    </div>
                </div>
                <button type="submit">Register</button>
            </form>
            <div className="form-footer">
                <p>Already have an account? <a href="/login">Log in</a></p>
                <p><a href="/terms">Terms & Conditions</a> | <a href="/privacy">Privacy Policy</a></p>
            </div>
        </div>
    );
}

export default RegistrationForm;

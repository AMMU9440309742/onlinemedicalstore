
import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, Carousel, CarouselItem, CarouselControl, CarouselIndicators } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './medicine.css';
import Paracetamol from '../medicineimages/paracetemol.jpg';
import coughsyrup from '../medicineimages/coughsyrup.jpg';
import mask from '../medicineimages/mask.jpeg';
import stayfree from '../medicineimages/stayfree.jpeg';
import Vitamin from '../medicineimages/vitaminc.jpg';
import ibuprofen from '../medicineimages/Ibuprofen.jpg';
import Amoxicillin from '../medicineimages/moxicillin.jpg';
import Lipitor from '../medicineimages/Liptor.jpg';
import Amlodipine from '../medicineimages/Amlodipine.jpg';
import Hydrochlorothiazide from '../medicineimages/Hydrochlorothiazide.jpg';
import Lisinopril from '../medicineimages/Lisinopril.jpg';
import Omeprazole from '../medicineimages/Omeprazole.jpg';
import Losartan from '../medicineimages/Losartan.jpg';
import Meloxicam from '../medicineimages/Meloxicam.jpg';
import headache from '../medicineimages/headache.jpeg';
import diper from '../medicineimages/diper.jpeg';
import '../App.css';
import 'font-awesome/css/font-awesome.min.css';
import C1 from '../medicineimages/c1.jpg';
import C3 from '../medicineimages/c3.jpg';
import C5 from '../medicineimages/c5.jpg';
import Card1 from '../medicineimages/card1.jpg';
import card2 from '../medicineimages/card2.jpg';
import card3 from '../medicineimages/card3.jpg';
import card4 from '../medicineimages/card4.jpg';
import card5 from '../medicineimages/card5.jpg';
import card6 from '../medicineimages/card6.jpg';
import card7 from '../medicineimages/card7.jpg';
import card8 from '../medicineimages/card8.jpg';
import card9 from '../medicineimages/card9.jpg';
import card10 from '../medicineimages/card10.jpg';

const medicines = [
  { id: 1, name: 'Paracetamol', image: Paracetamol },
  { id: 2, name: 'Cough Syrup', image: coughsyrup },
  { id: 3, name: 'Vitamin C', image: Vitamin },
  { id: 4, name: 'Ibuprofen', image: ibuprofen },
  { id: 5, name: 'Amoxicillin', image: Amoxicillin },
  { id: 6, name: 'Lipitor', image: Lipitor },
  { id: 7, name: 'Amlodipine', image: Amlodipine },
  { id: 8, name: 'Hydrochlorothiazide', image: Hydrochlorothiazide },
  { id: 9, name: 'Lisinopril', image: Lisinopril },
  { id: 10, name: 'Omeprazole', image: Omeprazole },
  { id: 11, name: 'Losartan', image: Losartan },
  { id: 12, name: 'Meloxicam', image: Meloxicam },
  {id:13,name: 'Mask',image:mask},
  {id:14,name:'Stayfree',image:stayfree},
  {id:15,name:'headache',image:headache},
  {id:16,name:'diper',image:diper}
];
const cardImages = [
  Card1,
  card2,
  card3,
  card4,
  card5,
  card6,
  card7,
  card8,
  card9,
  card10,
];

const carouselImages = [
  { src: C1, altText: 'Slide 1' },
  { src: C3, altText: 'Slide 2' },
  { src: C5, altText: 'Slide 3' },
];

const MedicalStore = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [modal, setModal] = useState(false);
  const [selectedMedicine, setSelectedMedicine] = useState('');
  const [orderDetails, setOrderDetails] = useState({
    name: '',
    address: '',
    quantity: 1,
    number: '',
  });
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const toggle = () => setModal(!modal);

  // carousel methods
  const next = () => {
    if (animating) return;
    const nextIndex =
      activeIndex === carouselImages.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };

  const previous = () => {
    if (animating) return;
    const nextIndex =
      activeIndex === 0 ? carouselImages.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };
  const handleOrderNow = (medicineName) => {
    setSelectedMedicine(medicineName);
    toggle();
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setOrderDetails((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const resetForm = () => {
    setOrderDetails({
      name: '',
      address: '',
      quantity: 1,
      number: '',
    });
  };

  const submitOrder = () => {
    console.log('Order Details:', orderDetails);
    resetForm();
    toggle();
  };

  const slides = carouselImages.map((image, index) => (
    <CarouselItem
      onExiting={() => setAnimating(true)}
      onExited={() => setAnimating(false)}
      key={index}
    >
      <img src={image.src} alt={image.altText} width="100%" height="500px" />
    </CarouselItem>
  ));

  return (
    <div>
      <Carousel
        activeIndex={activeIndex}
        next={next}
        previous={previous}
        ride="carousel"
        interval={2000}
      >
        <CarouselIndicators
          items={carouselImages}
          activeIndex={activeIndex}
          onClickHandler={goToIndex}
        />
        {slides}
        <CarouselControl
          direction="prev"
          directionText="Previous"
          onClickHandler={previous}
        />
        <CarouselControl
          direction="next"
          directionText="Next"
          onClickHandler={next}
        />
      </Carousel>

      <div className="search-container">
        <input
          className="search-input"
          type="text"
          placeholder="Search here for a medicine"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <i className="fa fa-search search-icon"></i>
      </div>

      <div className="medicine-cards">
        {medicines
          .filter((medicine) =>
            medicine.name.toLowerCase().includes(searchTerm.toLowerCase())
          )
          .map((filteredMedicine) => (
            <div key={filteredMedicine.id} className="medicine-card">
              <img
                src={filteredMedicine.image}
                alt={filteredMedicine.name}
              />
              <p>{filteredMedicine.name}</p>
              <Button
                color="primary"
                onClick={() => handleOrderNow(filteredMedicine.name)}
              >
                Order Now
              </Button>
            </div>
          ))}
      </div>

      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Order {selectedMedicine}</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="name">Full Name</Label>
              <Input
                type="text"
                name="name"
                id="name"
                placeholder="Enter your full name"
                value={orderDetails.name}
                onChange={handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="address">Delivery Address</Label>
              <Input
                type="text"
                name="address"
                id="address"
                placeholder="Enter your delivery address"
                value={orderDetails.address}
                onChange={handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="number">Phone Number</Label>
              <Input
                type="number"
                minLength={10}
                maxLength={12}
                name="number"
                id="number"
                placeholder="enter your contact number"
                value={orderDetails.number}
                onChange={handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="quantity">Quantity</Label>
              <Input
                type="number"
                name="quantity"
                id="quantity"
                min="1"
                value={orderDetails.quantity}
                onChange={handleInputChange}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={submitOrder}>
            Confirm Order
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>

      <footer className="store-footer">
        <div className="footer-content">
          <div className="about-store">
            <h4>About Our Online Medical Store</h4>
            <p>
              We're dedicated to providing a hassle-free online shopping
              experience for all your medical needs. Our platform offers a wide
              range of genuine medical products at competitive prices, ensuring
              home delivery for your utmost convenience. The safety and
              confidentiality of your transactions are our top priority.
            </p>
          </div>
          <div className="shopping-help">
            <h4>Shopping Help</h4>
            <ul>
              <li>
                <a href="#">How to Order</a>
              </li>
              <li>
                <a href="#">Payment Options</a>
              </li>
              <li>
                <a href="#">Refund & Return Policy</a>
              </li>
              <li>
                <a href="#">Shipping & Delivery</a>
              </li>
            </ul>
          </div>
          <div className="social-icons">
            <a
              href="https://www.facebook.com/mymedicalshop.official/"
              className="fa fa-facebook"
            ></a>
            <a
              href="https://twitter.com/i/flow/login?redirect_after_login=%2Fmedicalstore_IN"
              className="fa fa-twitter"
            ></a>
            <a
              href="https://www.instagram.com/pharmacitamal/"
              className="fa fa-instagram"
            ></a>
            <a
              href="https://www.youtube.com/watch?v=vvpyULWTw2w"
              className="fa fa-youtube"
            ></a>
            <a
              href="https://chat.whatsapp.com/invite/H6ej8BTHK360oP2EXWDhdF"
              className="fa fa-whatsapp"
            ></a>
          </div>
          <div className="store-info">
            <p>Medical Store, Main Street, City 12345</p>
            <p>Email: info@medicalstore.com</p>
            <p>Phone: (123) 456-7890</p>
          </div>
        </div>
      </footer>
    </div>
  );
};

export default MedicalStore;

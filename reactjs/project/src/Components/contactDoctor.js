import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import CardiologistImage from '../medicineimages/cardialagist.jpeg';
import PsychologistImage from '../medicineimages/physcolagist.jpeg';
import MigraineSpecialistImage from '../medicineimages/migrane.jpg';
import KidneySpecialistImage from '../medicineimages/kidney.jpeg';
import './contactDoctor.css';

const ContactDoctor = () => {
    const handleContactNow = (phoneNumber) => {
        const formattedPhoneNumber = phoneNumber.replace(/\s/g, ''); 
        const phoneUrl = `tel:${formattedPhoneNumber}`;
        window.location.href = phoneUrl;
    };

    const doctors = [
        {
            name: "Dr. John Doe",
            specialization: "Cardiologist",
            bio: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tincidunt urna eget lorem ultricies, ut fermentum dui convallis. Vestibulum id feugiat nisl. Sed ullamcorper dui ac augue tempus, eu fermentum odio finibus.",
            image: CardiologistImage,
            phoneNumber: "9440309742"
        },
        {
            name: "Dr. Jane Smith",
            specialization: "Psychologist",
            bio: "Nulla facilisi. Suspendisse bibendum mi sit amet enim eleifend, in tincidunt turpis feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean quis tellus ut tellus tincidunt sollicitudin. Nulla facilisi.",
            image: PsychologistImage,
            phoneNumber: "9777788777"
        },
        {
            name: "Dr. Mark Johnson",
            specialization: "Migraine Specialist",
            bio: "Duis vestibulum velit nec ligula luctus, vel accumsan elit finibus. Aliquam erat volutpat. Nunc ultrices varius nisl, eu dictum justo elementum in. Sed et elit ac metus dictum elementum.",
            image: MigraineSpecialistImage,
            phoneNumber: "9666699999"
        },
        {
            name: "Dr. Emily Davis",
            specialization: "Kidney Specialist",
            bio: "In hac habitasse platea dictumst. Vivamus ut dictum elit. Nunc fermentum nisl nec massa luctus, vel auctor quam consectetur. Duis eget nisl at turpis faucibus sagittis. Proin ultricies dolor et lectus efficitur, id feugiat velit ultricies.",
            image: KidneySpecialistImage,
            phoneNumber: "9555595555"
        }
    ];

    return (
        <Container>
            <h1>Contact Our Experts</h1>
            <Row>
                {doctors.map((doctor, index) => (
                    <Col md={6} key={index}>
                        <DoctorCard
                            name={doctor.name}
                            specialization={doctor.specialization}
                            bio={doctor.bio}
                            image={doctor.image}
                            phoneNumber={doctor.phoneNumber}
                            onContact={() => handleContactNow(doctor.phoneNumber)}
                        />
                    </Col>
                ))}
            </Row>
        </Container>
    );
}

const DoctorCard = ({ name, specialization, bio, image, phoneNumber, onContact }) => {
    return (
        <div className="DoctorCard">
            <img src={image} alt={name} />
            <h3>{name}</h3>
            <p><strong>Specialization:</strong> {specialization}</p>
            <p>{bio}</p>
            <button onClick={onContact}>Contact Now</button>
        </div>
    );
}

export default ContactDoctor;

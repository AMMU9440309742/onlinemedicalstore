import React, { useState } from 'react';

const Contact = () => {
    const [name, setName] = useState('');  // <-- Added this line
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');  // <-- Added this line

    const handleSubmit = e => {
        e.preventDefault();
        // Handle the contact form submission logic here
    }

    return (
        <div className="contact-container">
            <div className="contact-form">
                <h2>Contact Information</h2>
                <form onSubmit={handleSubmit}>
                    <div className="input-group">
                        <label>Name:</label>
                        <input type="text" value={name} onChange={e => setName(e.target.value)} required />
                    </div>
                    <div className="input-group">
                        <label>Email:</label>
                        <input type="email" value={email} onChange={e => setEmail(e.target.value)} required />
                    </div>
                    <div className="input-group">
                        <label>Message:</label>
                        <textarea value={message} onChange={e => setMessage(e.target.value)} required />
                    </div>
                    <button type="submit">Submit</button>
                </form>
            </div>
        </div>
    )
}

export default Contact;
